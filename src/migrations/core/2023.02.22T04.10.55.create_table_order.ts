import { Migration } from '@config/database/migration.provider';
import { UUIDV4 } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export const databasePath = __dirname;

export const up: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('orders', {
      id: {
        type: DataType.UUID,
        primaryKey: true,
        defaultValue: UUIDV4,
      },
      userId: {
        type: DataType.UUID,
        references: {
          key: 'id',
          model: 'users'
        },
      },
      number: {
        type: DataType.INTEGER,
        autoIncrement: true,
      },
      orderNumber: {
        type: DataType.STRING(50),
      },
      schedulesId: {
        type: DataType.UUID,
        allowNull: false
      },
      vehicleName: {
        type: DataType.STRING(100),
        allowNull: false
      },
      vehicleId: {
        type: DataType.UUID,
        allowNull: false
      },
      vehicleLicensePlate: {
        type: DataType.STRING(20),
        allowNull: false
      },
      vehicleCapacity: {
        type: DataType.INTEGER,
        allowNull: false
      },
      destinationCity: {
        type: DataType.STRING(50),
        allowNull: false
      },
      destinationTerminalName: {
        type: DataType.STRING,
        allowNull: false
      },
      destinationAddress: {
        type: DataType.STRING,
        allowNull: false
      },
      departureDate: {
        type: DataType.DATEONLY,
        allowNull: false
      },
      currentStatus: {
        type: DataType.STRING(20),
        allowNull: false
      },
      price: {
        type: DataType.DECIMAL,
        allowNull: false
      },
      qty: {
        type: DataType.DECIMAL,
        allowNull: false
      },
      total: {
        type: DataType.DECIMAL,
        allowNull: false
      },
      createdAt: DataType.DATE,
      createdBy: DataType.STRING,
      updatedAt: DataType.DATE,
      updatedBy: DataType.STRING,
      deletedAt: DataType.DATE,
      deletedBy: DataType.STRING,
      statusData: {
        type: DataType.STRING,
        defaultValue: "ACTIVE",
        allowNull: false,
      },
    });
  });
};
export const down: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('orders');
  });
};
