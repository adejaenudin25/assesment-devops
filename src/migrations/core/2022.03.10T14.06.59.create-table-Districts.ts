import { Migration } from '@config/database/migration.provider';
import { UUIDV4 } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export const databasePath = __dirname;

export const up: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('districts', {
      id: {
        type: DataType.UUID,
        primaryKey: true,
        defaultValue: UUIDV4,
      },
      name: {
        type: DataType.STRING,
        allowNull: false
      },
      cityId: {
        type: DataType.UUID,
        allowNull: false,
        references: {
          key: 'id',
          model: 'cities'
        },
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT'
      },
      createdAt: DataType.DATE,
      createdBy: DataType.STRING,
      updatedAt: DataType.DATE,
      updatedBy: DataType.STRING,
      deletedAt: DataType.DATE,
      deletedBy: DataType.STRING,
      statusData: {
        type: DataType.STRING,
        allowNull: false,
        defaultValue: "ACTIVE"
      },
    });
    
    await queryInterface.addIndex('districts', ['name']);
  });
};
export const down: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('districts');
  });
};
