import { Migration } from '@config/database/migration.provider';
import { UUIDV4 } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export const databasePath = __dirname;

export const up: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('schedules', {
      id: {
        type: DataType.UUID,
        primaryKey: true,
        defaultValue: UUIDV4,
      },
      vehicleId: {
        type: DataType.UUID,
        allowNull: false,
        references: {
          key: 'id',
          model: 'vehicles'
        },
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT'
      },
      destinationTo: {
        type: DataType.UUID,
        allowNull: false,
        references: {
          key: 'id',
          model: 'destinations'
        },
      },
      destinationFrom: {
        type: DataType.UUID,
        allowNull: false,
        references: {
          key: 'id',
          model: 'destinations'
        },
      },
      region: {
        type: DataType.STRING(100),
        allowNull: false,
      },
      departureDate: {
        type: DataType.DATEONLY,
        allowNull: false,
      },
      departureTime: {
        type: DataType.TIME,
        allowNull: false,
      },
      arrivalTime: {
        type: DataType.TIME,
        allowNull: false,
      },
      price: {
        type: DataType.DECIMAL,
        allowNull: false,
        defaultValue: 0
      },
      capacity: {
        type: DataType.DECIMAL,
        allowNull: false,
        defaultValue: 0
      },
      capacityAvailable: {
        type: DataType.DECIMAL,
        allowNull: false,
        defaultValue: 0
      },
      createdAt: DataType.DATE,
      createdBy: DataType.STRING,
      updatedAt: DataType.DATE,
      updatedBy: DataType.STRING,
      deletedAt: DataType.DATE,
      deletedBy: DataType.STRING,
      statusData: {
        type: DataType.STRING,
        defaultValue: "ACTIVE",
        allowNull: false,
      },
    });
  });
};
export const down: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('schedules');
  });
};
