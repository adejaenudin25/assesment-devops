import { Migration } from '@config/database/migration.provider';
import { UUIDV4 } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export const databasePath = __dirname;

export const up: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('users', {
      id: {
        type: DataType.UUID,
        primaryKey: true,
        defaultValue: UUIDV4
      },
      fullname: {
        type: DataType.STRING(200),
        allowNull: false
      },
      email: {
        type: DataType.STRING(200),
        allowNull: false
      },
      password: {
        type: DataType.STRING,
        allowNull: false
      },
      salt: {
        type: DataType.STRING,
        allowNull: true
      },
      phoneNumber: DataType.STRING,
      lastLogin: DataType.DATE,
      gender: DataType.STRING,
      address: DataType.STRING,
      profileImage: DataType.STRING,
      isAccountLocked: {
        type: DataType.BOOLEAN,
        defaultValue: false,
      },
      lockedReason: DataType.STRING,
      dateOfBirth: DataType.DATEONLY,
      timezone: DataType.STRING(200),
      createdAt: DataType.DATE,
      createdBy: DataType.STRING,
      updatedAt: DataType.DATE,
      updatedBy: DataType.STRING,
      deletedAt: DataType.DATE,
      deletedBy: DataType.STRING,
      statusData: {
        type: DataType.STRING,
        allowNull: false,
        defaultValue: "ACTIVE"
      },
    });

    await queryInterface.addIndex('users', ['fullname', 'email', 'gender', 'address', 'statusData', 'timezone']);
  });
};
export const down: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('users');
  });
};
