import { Migration } from '@config/database/migration.provider';
import { UUIDV4 } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export const databasePath = __dirname;

export const up: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('userRoles', {
      userId: {
        type: DataType.UUID,
        primaryKey: true,
        references: {
          key: 'id',
          model: 'users'
        },
        onDelete: 'CASCADE',
        onUpdate: 'RESTRICT'
      },
      roleId: {
        type: DataType.UUID,
        primaryKey: true,
        references: {
          key: 'id',
          model: 'roles'
        },
        onDelete: 'RESTRICT',
        onUpdate: 'RESTRICT'
      },
      createdAt: DataType.DATE,
      createdBy: DataType.STRING,
      updatedAt: DataType.DATE,
      updatedBy: DataType.STRING,
      deletedAt: DataType.DATE,
      deletedBy: DataType.STRING,
      statusData: {
        type: DataType.STRING,
        allowNull: false,
        defaultValue: "ACTIVE"
      },
    });
  });
};
export const down: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('userRoles');
  });
};
