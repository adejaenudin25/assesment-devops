import { Migration } from '@config/database/migration.provider';
import { UUIDV4 } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export const databasePath = __dirname;

export const up: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('orderItems', {
      id: {
        type: DataType.UUID,
        primaryKey: true,
        defaultValue: UUIDV4,
      },
      orderId: {
        type: DataType.UUID,
        references: {
          key: 'id',
          model: 'orders'
        },
        onDelete: 'cascade',
        onUpdate: 'restrict',
      },
      ageInDay: {
        type: DataType.DECIMAL
      },
      seatNumber: {
        type: DataType.SMALLINT
      },
      nik: {
        type: DataType.STRING(50)
      },
      name: {
        type: DataType.STRING
      },
      phoneNumber: {
        type: DataType.STRING(20)
      },
      address: {
        type: DataType.STRING
      },
      email: {
        type: DataType.STRING(50)
      },
      currentStatus: {
        type: DataType.STRING(20)
      },
      price: {
        type: DataType.DECIMAL
      },
      qty: {
        type: DataType.DECIMAL
      },
      total: {
        type: DataType.DECIMAL
      },
      createdAt: DataType.DATE,
      createdBy: DataType.STRING,
      updatedAt: DataType.DATE,
      updatedBy: DataType.STRING,
      deletedAt: DataType.DATE,
      deletedBy: DataType.STRING,
      statusData: {
        type: DataType.STRING,
        defaultValue: "ACTIVE",
        allowNull: false,
      },
    });
  });
};
export const down: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('orderItems');
  });
};
