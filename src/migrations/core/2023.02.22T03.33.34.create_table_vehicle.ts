import { Migration } from '@config/database/migration.provider';
import { UUIDV4 } from 'sequelize';
import { DataType } from 'sequelize-typescript';

export const databasePath = __dirname;

export const up: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.createTable('vehicles', {
      id: {
        type: DataType.UUID,
        primaryKey: true,
        defaultValue: UUIDV4,
      },
      name: {
        type: DataType.STRING(100),
        allowNull: false,
      },
      licensePlate: {
        type: DataType.STRING(20),
        allowNull: false,
      },
      capacity: {
        type: DataType.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      description: {
        type: DataType.STRING,
        allowNull: true,
      },
      createdAt: DataType.DATE,
      createdBy: DataType.STRING,
      updatedAt: DataType.DATE,
      updatedBy: DataType.STRING,
      deletedAt: DataType.DATE,
      deletedBy: DataType.STRING,
      statusData: {
        type: DataType.STRING,
        defaultValue: "ACTIVE",
        allowNull: false,
      },
    });
  });
};
export const down: Migration = async ({ context: queryInterface }) => {
  await queryInterface.sequelize.transaction(async (transaction) => {
    await queryInterface.dropTable('vehicles');
  });
};
