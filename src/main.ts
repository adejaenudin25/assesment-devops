import { ValidationPipe, VersioningType } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { AllExceptionsFilter } from '@utils/all-exception-filter';
import { install } from 'source-map-support';

// import { ValidationPipe } from '@utils/pipe/ValidationPipe';

import {
    SwaggerModule,
    DocumentBuilder,
    SwaggerCustomOptions,
} from '@nestjs/swagger';

import { AppModule } from './app.module';
import { TransformInterceptor } from '@utils/interceptors/transform.interceptor';
import { readFileSync } from 'fs';
import * as firebase from 'firebase-admin';
import * as moment from "moment-timezone";
import { AppClusterService } from 'app-cluster.service';

async function bootstrap() {
    install();

    moment.tz.setDefault("UTC");

    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    const configService = app.get(ConfigService);

    app.useGlobalPipes(new ValidationPipe());
    app.useGlobalFilters(new AllExceptionsFilter());
    app.useGlobalInterceptors(new TransformInterceptor());

    app.enableVersioning({
        type: VersioningType.URI,
    });

    app.setGlobalPrefix('api');

    app.enableCors({
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        allowedHeaders: '*',
        credentials: false,
    });

    const config = new DocumentBuilder()
        .setTitle('BOOKING TRAVEL API')
        .setDescription(
            'Api Project created by LabKlinik <div><a href="#">LabKlinik HQ - Website</a><div> <div><a href="mailto:info@example.com">Send email to Lab Klinik HQ</a></div>',
        )
        .setVersion('1.0')
        .addBearerAuth({
            // I was also testing it without prefix 'Bearer ' before the JWT
            description: `[just text field] Please enter token in following format: <JWT>`,
            name: 'Authorization',
            bearerFormat: 'Bearer', // I`ve tested not to use this field, but the result was the same
            scheme: 'Bearer',
            type: 'http', // I`ve attempted type: 'apiKey' too
            in: 'Header',
        })
        .build();
    const document = SwaggerModule.createDocument(app, config);

    const customOptions: SwaggerCustomOptions = {
        swaggerOptions: {
            persistAuthorization: true,
        },
        customSiteTitle: 'LABKLINIK API Docs',
    };

    SwaggerModule.setup('swagger/docs', app, document, customOptions);

    const appPort = configService.get<number>('app.port');

    //set public folder static
    app.useStaticAssets(
        configService.get<string>('app.uploadedFileDestination'),
        {
            prefix: '/shared/',
        },
    );
    await app.listen(appPort);
    console.log('>>> DEFAULT TIMEZONE', moment().toDate());
}
// bootstrap();

AppClusterService.clusterize(bootstrap);