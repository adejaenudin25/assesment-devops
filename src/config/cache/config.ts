import { registerAs } from '@nestjs/config';

export default registerAs('cache', () => ({
  CACHE_HOST: process.env.CACHE_HOST,
  CACHE_PORT: process.env.CACHE_PORT,
  CACHE_TTL: process.env.CACHE_TTL,
  CACHE_PREFIX: process.env.CACHE_PREFIX,
  CACHE_PASSWORD: process.env.CACHE_PASSWORD,
}));
