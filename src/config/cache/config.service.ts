import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class RedisConfigService {
  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  public async get(key: string) {
    return await this.cacheManager.get(key);
  }
  public async set(key: string, value: object, ttl: number) {
    return await this.cacheManager.set(key, value, { ttl });
  }
  public async del(key: string) {
    return await this.cacheManager.del(key);
  }
}
