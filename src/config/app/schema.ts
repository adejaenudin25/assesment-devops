import * as Joi from 'joi';
import { join } from 'path';
export default Joi.object({
  ENV: Joi.string().valid('development', 'production', 'uat').required(),
  PORT: Joi.number().default(3000),
  UPLOADED_FILES_DESTINATION: Joi.string().required(),
});
