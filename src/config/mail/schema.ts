import * as Joi from 'joi';
export default Joi.object({
  MAIL_MAILER: Joi.string().required(),
  MAIL_HOST: Joi.string().required(),
  MAIL_PORT: Joi.number().required(),
  MAIL_USERNAME: Joi.string().required(),
  MAIL_PASSWORD: Joi.string().required(),
  MAIL_ENCRYPTION: Joi.string().required(),
});
