import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MailConfigService {
  constructor(private readonly configService: ConfigService) {}

  get mailMailer(): string {
    return this.configService.get<string>('mail.MAIL_MAILER');
  }
  get mailHost(): string {
    return this.configService.get<string>('mail.MAIL_HOST');
  }
  get mailPort(): number {
    return this.configService.get<number>('mail.MAIL_PORT');
  }
  get mailUsername(): string {
    return this.configService.get<string>('mail.MAIL_USERNAME');
  }
  get mailPassword(): string {
    return this.configService.get<string>('mail.MAIL_PASSWORD');
  }
  get emailRecipients(): string[] {
    return [
      'imuni.logistic1@gmail.com',
      'imuni.logistic2@gmail.com',
      'imuni.logistic3@gmail.com',
      'imuni.logistic4@gmail.com',
    ];
  }
}
