// import {
//   Controller,
//   Post,
//   Get,
//   Body,
//   Put,
//   UseInterceptors,
//   Query,
//   Param,
//   Delete,
// } from '@nestjs/common';

// import { ApiOperation, ApiTags } from '@nestjs/swagger';
// import { Authorize } from '@utils/decorators/authorize.decorator';
// import { UserAppService } from '../services/user.app.service';

// @Authorize()
// @ApiTags('User')
// @Controller({ path: 'user', version: '1' })
// export class UserAppController {
//   constructor(private readonly userAppService: UserAppService) {}

//   @ApiOperation({ summary: 'Menampilkan List User' })
//   @UseInterceptors(new ResponsePaginationInterceptor('data'))
//   @Get()
//   async index(@Query() request: GetAllUserAppRequest) {
//     return this.userAppService.get(request);
//   }

//   @ApiOperation({ summary: 'Menampilkan Detail User' })
//   @Get(':userId')
//   async getUserDetail(@Param('userId') userId: string) {
//     return this.userAppService.getDetail(userId);
//   }

//   @ApiOperation({ summary: 'Membuat User Baru' })
//   @Post()
//   async createUser(@Body() command: CreateUserAppRequest) {
//     return this.userAppService.create(command);
//   }

//   @ApiOperation({ summary: 'Merubah User' })
//   @Put()
//   async editUserCms(@Body() command: EditUserAppRequest) {
//     return this.userAppService.edit(command);
//   }

//   @ApiOperation({ summary: 'Menghapus User' })
//   @Delete(':userId')
//   async deleteUser(@Param('userId') userId: string) {
//     return this.userAppService.delete(userId);
//   }
// }
