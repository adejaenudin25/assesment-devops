import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ScheduleController } from './controllers/schedule.controller';
import { ScheduleService } from './service/schedule.service';

@Module({
  imports: [HttpModule],
  providers: [
    ScheduleService,
  ],
  controllers: [ScheduleController],
})
export class ScheduleModule {}
