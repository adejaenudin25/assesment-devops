import {
  Controller,
  Get,
  UseInterceptors,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Authorize } from '@utils/decorators/authorize.decorator';
import { ResponsePaginationInterceptor } from '@utils/interceptors';
import { GetScheduleRequest } from '../request/get-schedule.request';
import { ScheduleService } from '../service/schedule.service';
import { Mutex } from 'async-mutex';

const mutex = new Mutex();

@ApiTags('Schedules')
@Authorize()
@Controller({ version: '1', path: 'schedule' })
export class ScheduleController {
  constructor(private readonly scheduleService: ScheduleService) {}

  @UseInterceptors(new ResponsePaginationInterceptor('data'))
  @Get()
  async getAll(@Query() request: GetScheduleRequest) {
    return await mutex.runExclusive(async () => {
      return await this.scheduleService.get(request);
    });
  }
}
