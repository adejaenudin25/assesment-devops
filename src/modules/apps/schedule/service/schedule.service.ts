import { City } from '@models/core/City';
import { Destination } from '@models/core/Destination';
import { Schedule } from '@models/core/Schedule';
import { Vehicle } from '@models/core/Vehicle';
import { Injectable } from '@nestjs/common';
import { basePagination } from '@utils/base-class/base.paginate';
import { Sequelize } from 'sequelize-typescript';
import { GetScheduleRequest } from '../request/get-schedule.request';

@Injectable()
export class ScheduleService {
  constructor(private readonly sequelize: Sequelize) {}

  async get(request: GetScheduleRequest) {
    var pagination = new basePagination(request.page, request.size);

    const schedules = Schedule.findAndCountAll({
      include: [
        {
          model: Vehicle,
          attributes: ['name', 'licensePlate', 'capacity'],
        },
        {
          model: Destination,
          as: 'myDestinationTo',
          attributes: ['code', 'terminalName', 'address'],
          include: [
            {
              model: City,
              attributes: ['id', 'name']
            }
          ]
        },
        {
          model: Destination,
          as: 'myDestinationFrom',
          attributes: ['code', 'terminalName', 'address'],
          include: [
            {
              model: City,
              attributes: ['id', 'name']
            }
          ]
        }
      ],
      offset: pagination.getPage(),
      limit: pagination.getSize(),
      order: this.orderBy(request?.sortBy, request?.sortDirection),
    });

    return schedules;
  }

  orderBy(sortBy: string, sortDirection: string): any {
    if (!sortDirection) {
      sortDirection = 'desc';
    }

    const orderBy =
      {
        destinationto: 'destinationTo',
        destinationfrom: 'destinationFrom',
        region: 'region',
        departuretime: 'departureTime',
        arrivaltime: 'arrivalTime',
        price: 'price',
        capacity: 'capacity',
        capacityavailable: 'capacityAvailable',
      }[sortBy?.toLowerCase()] ?? 'createdAt';

    return [
      [
        orderBy,
        sortDirection?.toLowerCase() == 'asc'
          ? `${sortDirection} NULLS FIRST`
          : `${sortDirection} NULLS LAST`,
      ],
    ];
  }
}
