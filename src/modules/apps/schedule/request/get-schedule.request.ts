import { ApiProperty } from '@nestjs/swagger';
import { IPaginate } from '@utils/base-class/base.paginate';
import { IsNotEmpty, IsNumberString, IsOptional, IsString } from 'class-validator';

enum sortByFill {
  destinationTo = 'destinationTo',
  destinationFrom = 'destinationFrom',
  region = 'region',
  departureTime = 'departureTime',
  arrivalTime = 'arrivalTime',
  price = 'price',
  capacity = 'capacity',
  capacityAvailable = 'capacityAvailable',
}

export class GetScheduleRequest implements IPaginate {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  page: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  size: number;

  @ApiProperty({ required: false, enum: sortByFill })
  @IsOptional()
  @IsString()
  sortBy: string;

  @ApiProperty({ required: false })
  @IsOptional()
  @IsString()
  sortDirection: string;
}
