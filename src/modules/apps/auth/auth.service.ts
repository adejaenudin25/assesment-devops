import { AuthConfigService } from '@config/auth/config.provider';
import { Role } from '@models/core/Role';
import { User } from '@models/core/User';
import { UserRole } from '@models/core/UserRole';
import {
  BadRequestException,
  Injectable,
  Req,
  Request,
  UnauthorizedException,
} from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AUTH } from '@utils/constant';
import { ROLE, STATUSDATA } from '@utils/enum';
import { AuthProvider } from 'modules/_common/auth/provider.service';
import { ILoginSocialPayload } from './interface/login-social.interface';
import { ConfigService } from '@nestjs/config';
import { firstValueFrom, lastValueFrom, map, tap } from 'rxjs';
import axios from 'axios';
import qs from 'qs';
import { use } from 'passport';
import { join } from 'path';
import { Sequelize } from 'sequelize-typescript';
import { Transaction } from 'sequelize';
import { RoleModule } from '@models/core/RoleModule';
import { RoleModulePermission } from '@models/core/RoleModulePermission';
import { Op } from 'sequelize';
import * as moment from 'moment';
import { ILogin, ILoginPayload } from './interface/login.interface';
import { compare, genSalt, hash } from 'bcrypt';
import { RegisterRequest } from './request/register.request';

@Injectable()
export class AuthService {
  constructor(
    private readonly commonAuthProvider: AuthProvider,
    private readonly authConfig: AuthConfigService,
    private readonly sequelize: Sequelize,
  ) {}

  async register(request: RegisterRequest, @Req() req) {
    return this.sequelize.transaction(async (transaction) => {
      const salt = await genSalt(10);
      const passwordHashed = await hash(request.password, salt);

      const [user, isCreated] = await User.findOrCreate({
        defaults: {
          fullname: request.fullname,
          email: request.email,
          password: passwordHashed,
          salt: salt,
          phoneNumber: request?.phoneNumber,
          gender: request.gender,
          address: request.address,
          isAccountLocked: false,
          dateOfBirth: request?.dateOfBirth,
          timezone: req.headers?.timezone ?? 'UTC'
        },
        where: {
          email: request.email
        },
        transaction
      });

      if (!isCreated) {
        throw new BadRequestException('email is already being used by another user');
      }

      const [role, isRoleCreated] = await Role.findOrCreate({
        defaults: {
          name: ROLE.USER
        },
        where: {
          name: ROLE.USER
        }
      });

      await UserRole.create({
        roleId: role.id,
        userId: user.id,
      }, {transaction});
  
      const loginPayload: ILoginPayload = {
        userLoginId: user.id,
        email: user.email,
      };
  
      return this.commonAuthProvider.createToken(
        {
          payload: loginPayload,
          key: this.authConfig.secret,
          audience: AUTH.AUDIENCE_APP,
        },
      );
    });

  }

  async login({ email, password }: ILogin) {
    const userLogin = await User
      .scopes('active')
      .findOneCache({
        ttl: 1,
        where: {
          email,
        },
        rejectOnEmpty: new BadRequestException('email or password not found or invalid'),
      });

    const isPasswordSame = await compare(password, userLogin.password);
    if (!isPasswordSame) throw new BadRequestException('email or password not found or invalid');

    const loginPayload: ILoginPayload = {
      userLoginId: userLogin.id,
      email: userLogin.email,
    };

    return this.commonAuthProvider.createToken(
      {
        payload: loginPayload,
        key: this.authConfig.secret,
        audience: AUTH.AUDIENCE_APP,
      },
    );
  }

  async me(@Request() req: any) {
    let user = await User.findOne({
      attributes: { exclude: ['createdAt', 'createdBy', 'updatedAt', 'updatedBy', 'deletedAt', 'deletedBy'] },
      where: {
        id: req.user.userLoginId,
      },
      include: [
        {
          model: Role,
          required: false,
          attributes: ['id', 'name'],
          include: [
            {
              model: RoleModule,
              required: false,
              attributes: ['id', 'roleId', 'moduleId'],
              include: [
                {
                  model: RoleModulePermission,
                  required: false,
                  attributes: [
                    'roleModuleId',
                    'permissionType',
                  ],
                },
              ],
            },
          ],
        },
      ],
    });

    return user;
  }
}
