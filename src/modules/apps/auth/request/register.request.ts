import { ApiProperty } from '@nestjs/swagger';
import { GENDER } from '@utils/enum';
import { IsEmail, IsEnum, IsNotEmpty, IsNumberString, IsOptional, IsPhoneNumber, IsString, MaxLength, MinLength } from 'class-validator';
import { ICreateUser } from '../interface/create-user.interfaces';

export class RegisterRequest implements ICreateUser {
  @ApiProperty()  
  @IsNotEmpty()
  fullname: string;
  
  @ApiProperty()  
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty()  
  @IsNotEmpty()
  password: string;
  
  @ApiProperty()  
  @IsNotEmpty()
  phoneNumber: string;
  
  @ApiProperty()  
  @IsNotEmpty()
  @IsEnum(GENDER)
  gender: GENDER;

  @ApiProperty()  
  @IsNotEmpty()
  address?: string;

  @ApiProperty()  
  @IsOptional()
  dateOfBirth?: Date;
}
