export interface ILoggedUser {
  userLoginId: string
  username: string
}
