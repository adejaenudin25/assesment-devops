export interface IPreLogin {
  email: string
}

export interface ILogin extends IPreLogin {
  password: string
}

export interface ILoginPayload {
  userLoginId: string
  email: string
}
