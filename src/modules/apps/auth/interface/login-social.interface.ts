  export interface ILoginSocial {
    userId: string
  }
  
  export interface ILoginSocialPayload {
    userLoginId: string
    username: string
  }
  