export interface ICreateUser {
    fullname: string
    gender: string
    email: string
    phoneNumber: string
}