import {
  Controller,
  UseGuards,
  Post,
  Req,
  Get,
  Body,
  Request,
  Put,
  UseInterceptors,
  UploadedFile,
  Res,
} from '@nestjs/common';

import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiProperty,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Authorize } from '@utils/decorators/authorize.decorator';
import { AuthService } from './auth.service';
import { LoginRequest } from './request/login.request';
import { RegisterRequest } from './request/register.request';
import { Mutex } from 'async-mutex';

const mutex = new Mutex();

@ApiTags('Auth')
@Controller({ path: 'auth', version: '1' })
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {}

  @Post('register/user')
  async userRegister(@Body() request: RegisterRequest, @Req() req) {
    return await mutex.runExclusive(async () => {
      return await this.authService.register(request, req);
    });
  }

  @Post('login')
  async login(@Body() request: LoginRequest) {
    return await mutex.runExclusive(async () => {
      return await this.authService.login(request);
    });
  }

  @Authorize()
  @Get('me')
  async me(@Request() req: any) {
    const result = await this.authService.me(req);
    return result;
  }
}
