import { Expose } from 'class-transformer';

export class AuthViewModel {
  @Expose()
  token: string;
}
