import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { OrderController } from './controllers/order.controller';
import { OrderService } from './service/order.service';

@Module({
  imports: [HttpModule],
  providers: [
    OrderService,
  ],
  controllers: [OrderController],
})
export class OrderModule {}
