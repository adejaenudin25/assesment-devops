import { ApiProperty } from '@nestjs/swagger';
import { ArrayMinSize, IsDateString, IsEmail, IsNotEmpty, IsNumber, IsNumberString, IsString } from 'class-validator';

export class OrderItemRequest {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumberString()
  seatNumber: number;

  @ApiProperty()
  @IsNotEmpty()
  nik: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsDateString()
  dateOfBirth: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  phoneNumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  address: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;
}

export class CreateOrderRequest {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  id: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  userId: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  schedulesId: string;

  @ApiProperty({ type: [OrderItemRequest] })
  @ArrayMinSize(1, {
    message: 'order item cannot be empty',
  })
  items: OrderItemRequest[];
}