import { City } from '@models/core/City';
import { Destination } from '@models/core/Destination';
import { Order } from '@models/core/Order';
import { OrderItem } from '@models/core/OrderItem';
import { Schedule } from '@models/core/Schedule';
import { Vehicle } from '@models/core/Vehicle';
import { BadRequestException, Injectable, Req } from '@nestjs/common';
import { STATUS_ORDER } from '@utils/enum/status-order.enum';
import { getAgeInDay, romanize } from '@utils/helper';
import moment from 'moment';
import { Op, Transaction } from 'sequelize';
import { Sequelize } from 'sequelize-typescript';
import { CreateOrderRequest } from '../request/create-order.request';

@Injectable()
export class OrderService {
  constructor(private readonly sequelize: Sequelize) {}

  async create(request: CreateOrderRequest, @Req() req) {
    const schedule = await Schedule.findOne({
      where: {
        id: request.schedulesId,
      },
      include: [
        {
          model: Vehicle,
          attributes: ['id', 'capacity'],
        },
        {
          model: Destination,
          attributes: ['id', 'code', 'terminalName', 'address'],
          include: [
            {
              model: City,
              attributes: ['id', 'name']
            }
          ]
        },
      ],
      rejectOnEmpty: new BadRequestException('schedule not found'),
    });

    if (schedule.capacityAvailable <= 0) {
      throw new BadRequestException('schedule is full');
    }

    if (this.isSeatUsed(request)) {
      throw new BadRequestException('some selected seats are already booked');
    }

    return this.sequelize.transaction(async (transaction) => {
      const order = await this.startCreateOrder(request, schedule, req.user.userLoginId, transaction);

      await this.startCreateOrderItems(request, schedule, order, transaction);

      await this.startUpdateCapacity(schedule, order, transaction);

      return order;
    });
  }

  async startUpdateCapacity(schedule: Schedule, order: Order, transaction: Transaction) {
    schedule.capacityAvailable = schedule.capacityAvailable - order.qty;
    await schedule.save({transaction});
  }

  async startCreateOrderItems(request: CreateOrderRequest, schedule: Schedule, order: Order, transaction: Transaction) {
    const items = []
    for(const item of request.items) {
      items.push({
        orderId: order.id,
        ageInDay: getAgeInDay(item.dateOfBirth, new Date()),
        seatNumber: item.seatNumber,
        nik: item.nik,
        name: item.name,
        phoneNumber: item.phoneNumber,
        address: item.address,
        email: item.email,
        currentStatus: order.currentStatus,
        price: schedule.price,
        qty: 1,
        total: schedule.price * 1
      });
    }

    if (items.length > 0) {
      await OrderItem.bulkCreate(items, {transaction});
    }
  }

  async startCreateOrder(request: CreateOrderRequest, schedule: Schedule, userId: string, transaction: Transaction) {
    const order = await Order.create({
      userId: userId,
      schedulesId: request.schedulesId,
      vehicleId: schedule.vehicle.id,
      vehicleName: schedule.vehicle.name,
      vehicleLicensePlate: schedule.vehicle.licensePlate,
      vehicleCapacity: schedule.vehicle.capacity,
      destinationCity: schedule.myDestinationTo.city.name,
      destinationTerminalName: schedule.myDestinationTo.terminalName,
      destinationAddress: schedule.myDestinationTo.address,
      departureDate: schedule.departureDate,
      currentStatus: STATUS_ORDER.DRAFT,
      price: schedule.price,
      qty: request.items.length,
      total: schedule.price * request.items.length
    }, {transaction});

    const dateNow = moment();

    order.orderNumber = `${order.number.toString().padStart(10, '0')}/TRVL/${schedule.myDestinationFrom.code.toUpperCase()}/${schedule.myDestinationTo.code.toUpperCase()}/${romanize(dateNow.month()+1)}/${dateNow.year()}`;
    await order.save({transaction});
    
    return order;
  }

  async isSeatUsed(request: CreateOrderRequest) {
    const orderAny = await  Order.count({ 
      where: { 
        schedulesId: request.schedulesId,
        '$orderItem.seatNumber$': {
          [Op.in]: request.items.map(m => m.seatNumber)
        }
       },
      include: [
        {
          model: OrderItem,
          attributes: ['seatNumber']
        }
      ],
    });

    return orderAny > 0;
  }
}
