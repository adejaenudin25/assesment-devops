import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Body,
  Post,
  Req,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { Authorize } from '@utils/decorators/authorize.decorator';
import { CreateOrderRequest } from '../request/create-order.request';
import { OrderService } from '../service/order.service';
import { Mutex } from 'async-mutex';

const mutex = new Mutex();

@ApiTags('Orders')
@Authorize()
@Controller({ version: '1', path: 'order' })
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Post()
  async createOrder(@Body() request: CreateOrderRequest, @Req() req) {
    return await mutex.runExclusive(async () => {
      return await this.orderService.create(request, req);
    });
  }
}
