import { Expose } from 'class-transformer';

export class UserViewModel {
  @Expose()
  argosUserLoginId: number;

  @Expose()
  username: string;
}
