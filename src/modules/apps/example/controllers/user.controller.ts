// import {
//   Controller, Get, Param, ParseIntPipe, UseGuards, UseInterceptors,
// } from '@nestjs/common';
// import { AuthGuard } from '@nestjs/passport';
// import { ApiTags } from '@nestjs/swagger';
// import { User as LoggedUser } from '@utils/decorators';
// import { generateViewModel } from '@utils/helper';

// import { UserViewModel } from '../viewmodel/user.viewmodel';

// @ApiTags('Examples')
// @UseGuards(AuthGuard('app-auth'))
// @Controller({ version: '1', path: 'user' })
// export class UserController {
//   @Get(':id')
//   // @Permissions(PERMISSION.CAN_VIEW_USER)
//   async getUser(@Param('id', ParseIntPipe) id: number, @LoggedUser() loggedUser): Promise<UserViewModel> {
//     // const user = await ArgosUserLogin.findOne();

//     return generateViewModel(UserViewModel, user);
//   }
// }
