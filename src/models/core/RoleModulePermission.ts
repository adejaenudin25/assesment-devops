import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { ROLE, STATUSDATA } from '@utils/enum';
import { UUIDV4 } from 'sequelize';
import {
  AllowNull,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  Column,
} from 'sequelize-typescript';
import { IsEnum } from 'class-validator';
import { BaseEntity } from './BaseEntity';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  roleModuleId: string;
  permissionType: string;
}

export type IModelCreate = Omit<IModel, ''>;

@DefaultScope(() => ({
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'roleModulePermissions',
  paranoid: true,
})
export class RoleModulePermission
  extends BaseEntity<IModel, IModelCreate>
  implements IModel
{
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'roleModuleId'
  })
  roleModuleId: string;

  @AllowNull(false)
  @IsEnum(ROLE)
  @Column({
    field: 'permissionType'
  })
  permissionType: string;
}
