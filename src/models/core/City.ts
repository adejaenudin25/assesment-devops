import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { UUIDV4 } from 'sequelize';
import { STATUSDATA } from '@utils/enum';
import {
    AllowNull,
    BelongsTo,
    BelongsToMany,
    Column,
    DataType,
    Default,
    DefaultScope,
    HasMany,
    PrimaryKey,
    Scopes,
    Table,
} from 'sequelize-typescript';
import { BaseEntity } from './BaseEntity';
import { Province } from './Province';
import { Cache } from 'base-repo';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
    id: string;
    name: string;
    provinceId: string;
}
export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
    where: {
        deletedAt: null,
    },
}))
@Scopes(() => ({
    active: {
        where: {
            deletedAt: null,
            statusData: STATUSDATA.ACTIVE,
        },
        order: [['name', 'ASC']],
    },
}))
@Cache()
@Table({
    tableName: 'cities',
    paranoid: true,
})
export class City extends BaseEntity<IModel, IModelCreate> implements IModel {
    @PrimaryKey
    @AllowNull(false)
    @Default(UUIDV4)
    @Column({
        field: 'id',
        type: DataType.UUID,
    })
    id: string;

    @AllowNull(false)
    @Column({
        field: 'provinceId',
        type: DataType.UUID,
    })
    provinceId: string;

    @AllowNull(false)
    @Column({
        field: 'name',
        type: DataType.STRING,
    })
    name: string;

    // relationship
    @BelongsTo(() => Province, 'provinceId')
    province: Province;
}
