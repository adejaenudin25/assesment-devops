import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  BelongsTo,
  BelongsToMany,
  DataType,
  Unique,
  ForeignKey,
  BeforeUpdate,
  BeforeCreate,
} from 'sequelize-typescript';
import { UUIDV4 } from 'sequelize';
import { City } from './City';
import { BaseEntity } from './BaseEntity';
import { STATUSDATA } from '@utils/enum';
import { Province } from './Province';
import { User } from './User';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  id?: string;
  name: any;
  cityId: string;
  provinceId: string;
}

export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
    order: [['name', 'ASC']],
  },
}))
@Table({
  tableName: 'districts',
  paranoid: true,
  indexes: [
    {
      fields: ['name'],
      unique: true,
      where: { deletedAt: null },
    },
    { fields: ['cityId', 'provinceId'] },
  ],
})
export class District
  extends BaseEntity<IModel, IModelCreate>
  implements IModel
{
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'districtId',
    type: DataType.UUID,
  })
  districtId: string;

  @ForeignKey(() => City)
  @AllowNull(false)
  @Column({ field: 'cityId' })
  cityId: string;

  @ForeignKey(() => Province)
  @AllowNull(false)
  @Column({ field: 'provinceId' })
  provinceId: string;

  @Unique(true)
  @Column({ field: 'name' })
  name: string;

  // relationship
  @BelongsTo(() => City, 'cityId')
  city: City;

  @BelongsTo(() => Province, 'provinceId')
  province: Province;
}
