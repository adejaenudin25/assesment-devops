import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  AutoIncrement,
  ForeignKey,
  DataType,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Role } from './Role';
import { BaseEntity } from './BaseEntity';
import { UUIDV4 } from 'sequelize';
import { City } from './City';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  code: string;
  cityId: string;
  terminalName: string;
  address: string;
}

export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
  attributes: {
    exclude: [
      'createdAt',
      'createdBy',
      'updatedAt',
      'updatedBy',
      'deletedAt',
      'deletedBy',
    ],
  },
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'destinations',
  paranoid: true,
})
export class Destination
  extends BaseEntity<IModel, IModelCreate>
  implements IModel
{
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'id',
    type: DataType.UUID,
  })
  id: string;

  @Column({
    field: 'code',
  })
  code: string;

  @ForeignKey(() => City)
  @Column({
    field: 'cityId',
  })
  cityId: string;

  @Column({
    field: 'terminalName',
  })
  terminalName: string;

  @Column({
    field: 'address',
  })
  address: string;

  // relationship
  @BelongsTo(() => City, 'cityId')
  city: City;
}
