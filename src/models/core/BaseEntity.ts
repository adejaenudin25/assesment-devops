import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import CurrentUserService from '@utils/helper/CurrentUserService';
import { Model } from 'base-repo';
import {
  Column,
  Default,
  Table,
  BeforeUpdate,
  BeforeDestroy,
  BeforeCreate,
} from 'sequelize-typescript';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {}

@Table({
  paranoid: true,
})
export class BaseEntity<T1, T2> extends Model<T1, T2> implements IModel {
  @Column({
    field: 'createdAt'
  })
  createdAt?: Date;

  @Column({
    field: 'createdBy'
  })
  createdBy?: string;

  @Column({
    field: 'updatedAt'
  })
  updatedAt?: Date;

  @Column({
    field: 'updatedBy'
  })
  updatedBy?: string;

  @Column({
    field: 'deletedAt'
  })
  deletedAt?: Date;

  @Column({
    field: 'deletedBy'
  })
  deletedBy?: string;

  @Default(STATUSDATA.ACTIVE)
  @Column({
    field: 'statusData'
  })
  statusData?: string;

  @BeforeCreate
  static setCreateEvent(instance: BaseEntity<null, null>) {
    instance.createdBy = CurrentUserService?.userLoginId;
  }

  @BeforeUpdate
  static setUpdateEvent(instance: BaseEntity<null, null>) {
    instance.updatedBy = CurrentUserService?.userLoginId;
  }

  @BeforeDestroy
  static setDestroyEvent(instance: BaseEntity<null, null>) {
    instance.deletedBy = CurrentUserService?.userLoginId;
  }
}
