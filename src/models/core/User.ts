import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import { UUIDV4 } from 'sequelize';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  DataType,
  BelongsToMany,
  HasMany,
  BeforeUpdate,
  IsEmail,
} from 'sequelize-typescript';
import { Role } from './Role';
import { UserRole } from './UserRole';
import { BaseEntity } from './BaseEntity';

interface IModelOptional {
  id: string;
  phoneNumber?: string;
  lastLogin?: Date;
  address?: string;
  profileImage?: string;
  token?: string;
  refreshToken?: string;
  refreshTokenExpired?: string;
  isAccountLocked?: boolean;
  lockedReason?: string;
  dateOfBirth?: Date;
  gender?: string;
  timezone?: string;
  salt?: string;
}

export interface IModel
  extends Partial<IUnfilledAtt>,
    Partial<IModelOptional>,
    Partial<IUnfilledBy> {
  fullname: string;
  email: string;
  password: string;
}

export type IModelCreate = Omit<IModel, 'id'> & Partial<IModelOptional>;

@DefaultScope(() => ({
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'users',
  paranoid: true,
})
export class User extends BaseEntity<IModel, IModelCreate> implements IModel {
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    type: DataType.UUID,
  })
  id: string;

  @AllowNull(false)
  @Column({
    field: 'fullname'
  })
  fullname: string;

  @AllowNull(false)
  @Column({
    field: 'email'
  })
  email: string;

  @AllowNull(false)
  @Column({
    field: 'password'
  })
  password: string;

  @AllowNull(true)
  @Column({
    field: 'salt'
  })
  salt: string;

  @Column({
    field: 'dateOfBirth'
  })
  dateOfBirth: Date;

  @Column({
    field: 'gender'
  })
  gender: string;

  @Column({
    field: 'phoneNumber'
  })
  phoneNumber?: string;

  @Column({
    field: 'lastLogin'
  })
  lastLogin?: Date;

  @Column({
    field: 'address'
  })
  address?: string;

  @Column({
    field: 'profileImage'
  })
  profileImage?: string;

  @Default(false)
  @Column({
    field: 'isAccountLocked'
  })
  isAccountLocked?: boolean;

  @Column({
    field: 'lockedReason'
  })
  lockedReason?: string;

  @Column({
    type: DataType.STRING(100),
    field: 'timezone'
  })
  timezone?: string;

  // relationship
  @BelongsToMany(() => Role, () => UserRole)
  roles: Role[];
}
