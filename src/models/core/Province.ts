import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { UUIDV4 } from 'sequelize';
import { STATUSDATA } from '@utils/enum';
import {
    AllowNull,
    Column,
    DataType,
    Default,
    DefaultScope,
    HasMany,
    PrimaryKey,
    Scopes,
    Table,
} from 'sequelize-typescript';
import { BaseEntity } from './BaseEntity';
import { City } from './City';
import { Cache } from 'base-repo';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
    id: string;
    name: string;
}
export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
    where: {
        deletedAt: null,
    },
}))
@Scopes(() => ({
    active: {
        where: {
            deletedAt: null,
            statusData: STATUSDATA.ACTIVE,
        },
    },
}))
@Cache()
@Table({
    tableName: 'provinces',
    paranoid: true,
})
export class Province
    extends BaseEntity<IModel, IModelCreate>
    implements IModel
{
    @PrimaryKey
    @AllowNull(false)
    @Default(UUIDV4)
    @Column({
        field: 'id',
        type: DataType.UUID,
    })
    id: string;

    @AllowNull(false)
    @Column({
        field: 'name',
        type: DataType.STRING,
    })
    name: string;

    // relationship
    @HasMany(() => City, 'provinceId')
    cities: City[];
}
