import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  AutoIncrement,
  ForeignKey,
  DataType,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { User } from './User';
import { Role } from './Role';
import { BaseEntity } from './BaseEntity';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  userId: string;
  roleId: string;
}

export type IModelCreate = Omit<IModel, ''>;

@DefaultScope(() => ({
  attributes: {
    exclude: [
      'createdAt',
      'createdBy',
      'updatedAt',
      'updatedBy',
      'deletedAt',
      'deletedBy',
    ],
  },
  where: {
    DeletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      DeletedAt: null,
      StatusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'userRoles',
  paranoid: true,
})
export class UserRole
  extends BaseEntity<IModel, IModelCreate>
  implements IModel
{
  @ForeignKey(() => User)
  @Column({
    field: 'userId',
  })
  userId: string;

  @ForeignKey(() => Role)
  @Column({
    field: 'roleId',
  })
  roleId: string;
}
