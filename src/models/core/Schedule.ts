import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  ForeignKey,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import { BaseEntity } from './BaseEntity';
import { UUIDV4 } from 'sequelize';
import { City } from './City';
import { Destination } from './Destination';
import { Vehicle } from './Vehicle';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  id: string;
  vehicleId: string;
  destinationTo: string;
  destinationFrom: string;
  region: string;
  departureDate: Date;
  departureTime: string;
  arrivalTime: string;
  price: number;
  capacity: number;
  capacityAvailable: number;
}

export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
  attributes: {
    exclude: [
      'createdAt',
      'createdBy',
      'updatedAt',
      'updatedBy',
      'deletedAt',
      'deletedBy',
    ],
  },
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'schedules',
  paranoid: true,
})
export class Schedule
  extends BaseEntity<IModel, IModelCreate>
  implements IModel
{
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'id',
    type: DataType.UUID,
  })
  id: string;

  @ForeignKey(() => Vehicle)
  @Column({
    field: 'vehicleId',
  })
  vehicleId: string;
  
  @ForeignKey(() => Destination)
  @Column({
    field: 'destinationTo',
  })
  destinationTo: string;

  @ForeignKey(() => Destination)
  @Column({
    field: 'destinationTo',
  })
  @Column({
    field: 'destinationFrom',
  })
  destinationFrom: string;

  @Column({
    field: 'region',
  })
  region: string;

  @Column({
    field: 'departureDate',
  })
  departureDate: Date;

  @Column({
    field: 'departureTime',
  })
  departureTime: string;

  @Column({
    field: 'arrivalTime',
  })
  arrivalTime: string;

  @Column({
    field: 'price',
    type: DataType.DECIMAL
  })
  price: number;

  @Column({
    field: 'capacity',
    type: DataType.DECIMAL
  })
  capacity: number;

  @Column({
    field: 'capacityAvailable',
    type: DataType.DECIMAL
  })
  capacityAvailable: number;

  // relationship
  @BelongsTo(() => Destination, 'destinationTo')
  myDestinationTo: Destination;

  @BelongsTo(() => Destination, 'destinationFrom')
  myDestinationFrom: Destination;

  @BelongsTo(() => Vehicle, 'vehicleId')
  vehicle: Vehicle;
}
