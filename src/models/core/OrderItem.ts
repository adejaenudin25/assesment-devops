import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  ForeignKey,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import { BaseEntity } from './BaseEntity';
import { UUIDV4 } from 'sequelize';
import { City } from './City';
import { Destination } from './Destination';
import { Vehicle } from './Vehicle';
import { User } from './User';
import { Order } from './Order';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  id: string;
  orderId: string;
  ageInDay: number;
  seatNumber: number;
  nik: string;
  name: string;
  phoneNumber: string;
  address: string;
  email: string;
  currentStatus: string;
  price: number;
  qty: number;
  total: number;
}

export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
  attributes: {
    exclude: [
      'createdAt',
      'createdBy',
      'updatedAt',
      'updatedBy',
      'deletedAt',
      'deletedBy',
    ],
  },
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'orderItems',
  paranoid: true,
})
export class OrderItem extends BaseEntity<IModel, IModelCreate> implements IModel {
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'id',
    type: DataType.UUID,
  })
  id: string;

  @Column({
    field: 'orderId'
  })
  orderId: string;
  
  @Column({
    field: 'ageInDay',
    type: DataType.DECIMAL
  })
  ageInDay: number;
  
  @Column({
    field: 'seatNumber'
  })
  seatNumber: number;
  
  @Column({
    field: 'nik'
  })
  nik: string;
  
  @Column({
    field: 'name'
  })
  name: string;
  
  @Column({
    field: 'phoneNumber'
  })
  phoneNumber: string;
  
  @Column({
    field: 'address'
  })
  address: string;
  
  @Column({
    field: 'email'
  })
  email: string;
  
  @Column({
    field: 'currentStatus'
  })
  currentStatus: string;
  
  @Column({
    field: 'price',
    type: DataType.DECIMAL
  })
  price: number;
  
  @Column({
    field: 'qty',
    type: DataType.DECIMAL
  })
  qty: number;
  
  @Column({
    field: 'total',
    type: DataType.DECIMAL
  })
  total: number;

  // relationship
  @BelongsTo(() => Order, 'orderId')
  order: Order;
}
