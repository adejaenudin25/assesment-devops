import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { ROLE, STATUSDATA } from '@utils/enum';
import { UUIDV4 } from 'sequelize';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  HasMany,
} from 'sequelize-typescript';
import { IsEnum } from 'class-validator';
import { BaseEntity } from './BaseEntity';
import { RoleModulePermission } from './RoleModulePermission';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  id: string;
  roleId: string;
  moduleId: string;
}

export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'roleModules',
  paranoid: true,
})
export class RoleModule
  extends BaseEntity<IModel, IModelCreate>
  implements IModel
{
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'id'
  })
  id: string;

  @AllowNull(false)
  @IsEnum(ROLE)
  @Column({
    field: 'roleId'
  })
  roleId: string;

  @AllowNull(false)
  @Column({
    field: 'moduleId'
  })
  moduleId: string;

  // relationship
  @HasMany(() => RoleModulePermission, 'roleModuleId')
  modulePermissions: RoleModulePermission[];
}
