import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  ForeignKey,
  DataType,
  BelongsTo,
} from 'sequelize-typescript';
import { BaseEntity } from './BaseEntity';
import { UUIDV4 } from 'sequelize';
import { City } from './City';
import { Destination } from './Destination';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  id: string;
  name: string;
  licensePlate: string;
  capacity: number;
  description: string;
}

export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
  attributes: {
    exclude: [
      'createdAt',
      'createdBy',
      'updatedAt',
      'updatedBy',
      'deletedAt',
      'deletedBy',
    ],
  },
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'vehicles',
  paranoid: true,
})
export class Vehicle
  extends BaseEntity<IModel, IModelCreate>
  implements IModel
{
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'id',
    type: DataType.UUID,
  })
  id: string;

  @Column({
    field: 'name',
  })
  name: string;

  @Column({
    field: 'licensePlate',
  })
  licensePlate: string;

  @Column({
    field: 'capacity',
    type: DataType.DECIMAL
  })
  capacity: number;

  @Column({
    field: 'description',
  })
  description: string;

  // relationship
  @BelongsTo(() => Destination, 'destinationTo')
  myDestinationTo: Destination;

  @BelongsTo(() => Destination, 'destinationFrom')
  myDestinationFrom: Destination;
}
