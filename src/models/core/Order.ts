import { IUnfilledAtt, IUnfilledBy } from '@utils/base-class/base.interface';
import { STATUSDATA } from '@utils/enum';
import {
  AllowNull,
  Column,
  Default,
  PrimaryKey,
  DefaultScope,
  Scopes,
  Table,
  ForeignKey,
  DataType,
  BelongsTo,
  HasMany,
} from 'sequelize-typescript';
import { BaseEntity } from './BaseEntity';
import { UUIDV4 } from 'sequelize';
import { User } from './User';
import { OrderItem } from './OrderItem';

export interface IModel extends Partial<IUnfilledAtt>, Partial<IUnfilledBy> {
  id: string;
  userId: string;
  number: number;
  orderNumber: string;
  schedulesId: string;
  vehicleId: string;
  vehicleName: string;
  vehicleLicensePlate: string;
  vehicleCapacity: number;
  destinationCity: string;
  destinationTerminalName: string;
  destinationAddress: string;
  departureDate: Date;
  currentStatus: string;
  price: number;
  qty: number;
  total: number;
}

export type IModelCreate = Omit<IModel, 'id'>;

@DefaultScope(() => ({
  attributes: {
    exclude: [
      'createdAt',
      'createdBy',
      'updatedAt',
      'updatedBy',
      'deletedAt',
      'deletedBy',
    ],
  },
  where: {
    deletedAt: null,
  },
}))
@Scopes(() => ({
  active: {
    where: {
      deletedAt: null,
      statusData: STATUSDATA.ACTIVE,
    },
  },
}))
@Table({
  tableName: 'orders',
  paranoid: true,
})
export class Order extends BaseEntity<IModel, IModelCreate> implements IModel {
  @PrimaryKey
  @AllowNull(false)
  @Default(UUIDV4)
  @Column({
    field: 'id',
    type: DataType.UUID,
  })
  id: string;

  @ForeignKey(() => User)
  @AllowNull(false)
  @Column({
    field: 'userId',
  })
  userId: string;

  @AllowNull(false)
  @Column({
    field: 'number',
  })
  number: number;

  @AllowNull(false)
  @Column({
    field: 'orderNumber',
  })
  orderNumber: string;

  @AllowNull(false)
  @Column({
    field: 'schedulesId',
  })
  schedulesId: string;

  @AllowNull(false)
  @Column({
    field: 'vehicleName',
  })
  vehicleName: string;

  @AllowNull(false)
  @Column({
    field: 'vehicleId',
  })
  vehicleId: string;

  @AllowNull(false)
  @Column({
    field: 'vehicleLicensePlate',
  })
  vehicleLicensePlate: string;

  @AllowNull(false)
  @Column({
    field: 'vehicleCapacity',
  })
  vehicleCapacity: number;

  @AllowNull(false)
  @Column({
    field: 'destinationCity',
  })
  destinationCity: string;

  @AllowNull(false)
  @Column({
    field: 'destinationTerminalName',
  })
  destinationTerminalName: string;

  @AllowNull(false)
  @Column({
    field: 'destinationAddress',
  })
  destinationAddress: string;

  @AllowNull(false)
  @Column({
    field: 'departureDate',
    type: DataType.DATEONLY
  })
  departureDate: Date;

  @AllowNull(false)
  @Column({
    field: 'currentStatus',
  })
  currentStatus: string;

  @AllowNull(false)
  @Column({
    field: 'price',
    type: DataType.DECIMAL
  })
  price: number;

  @AllowNull(false)
  @Column({
    field: 'qty',
    type: DataType.DECIMAL
  })
  qty: number;

  @AllowNull(false)
  @Column({
    field: 'total',
    type: DataType.DECIMAL
  })
  total: number;

  // relationship
  @BelongsTo(() => User, 'userId')
  user: User;

  @HasMany(() => OrderItem, 'orderId')
  orderItems: OrderItem[];
}
