import { ExampleModule } from '@apps/example/example.module';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AppsModule } from 'modules/apps/apps.module';

import { CONFIG_MODULES } from 'app.provider';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CommonModule } from './modules/_common/common.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    AppsModule,
    CommonModule,
    ExampleModule,
    ...CONFIG_MODULES,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
