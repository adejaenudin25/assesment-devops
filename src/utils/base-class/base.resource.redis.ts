export const resourceNames = ['schedule-time', 'schedule-date'] as const;

export type ResourceRedis = typeof resourceNames[number];

export class BaseResourceRedis {
  serializeName: ResourceRedis;

  constructor(resourceName: ResourceRedis) {
    this.serializeName = resourceName;
  }

  public key(args: any) {
    return `:${this.serializeName}:${args}`;
  }
}
