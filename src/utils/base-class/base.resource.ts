import * as JsonAPISerializer from 'json-api-serializer';

export const resourceNames = [
  'users',
  'data',
  'doctors',
  'provinces',
  'cities',
  'districts',
  'areas',
  'area-group',
  'schedule-time',
  'schedule-date',
  'vaccine',
  'vaccine-info',
  'vaccine-types',
  'vaccine-vendors',
  'orders',
  'order-apps',
  'vc-orders-apps',
  'payment',
  'payment-confirmation',
  'payment-manual',
  'payment-xendit',
  'dayops',
  'user-address',
  'user-patient',
  'registrant',
  'vaccine-history',
  'vaccine-reminder',
  'community-post',
  'medical-record',
  'order-fees',
] as const;

export type Resource = typeof resourceNames[number];

export class BaseResource {
  protected serializer = new JsonAPISerializer();

  /**
   * resource string name
   * @param resourceName
   * @param data
   */
  constructor(resourceName: Resource, data: any) {
    /**
     * @see {resource}
     * register all defined resource
     * */
    resourceNames.forEach((market: Resource) => {
      this.serializer.register(market, {
        id: 'id',
      });
    });

    return this.serializer.serialize(resourceName, data);
  }
}
