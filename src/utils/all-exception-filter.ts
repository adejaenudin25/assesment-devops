import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    HttpException,
    HttpStatus,
} from '@nestjs/common';
import * as JSONAPISerializer from 'json-api-serializer';
import * as moment from 'moment';

// tslint:disable-next-line:variable-name
// tslint:disable-next-line:variable-name
const Serializer = new JSONAPISerializer();

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    catch(exception: HttpException | any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        const request = ctx.getRequest();

        const isAcceptedApiV1 = request.url.includes('api/v1');

        // const user = isAcceptedApi ? request.user : 'ATTACK';
        const { url, method } = request;
        // const headers = request.headers;

        const status =
            exception instanceof HttpException || +exception?.getStatus?.()
                ? +exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;
        const stack = !exception.stack ? null : exception.stack;

        // console.log('\x1b[36m', stack, '\x1b[0m');
        const errorCode = (exception as any)?.response?.error || undefined;

        const errorMessage: any =
            (exception as any)?.response?.message ||
            exception?.message ||
            exception;

        // console.log(exception)

        let errorDefault: any = {
            statusCode: status,
            timestamp: moment().format('YYYY-MM-DD'),
            path: url,
            code: errorCode,
            message: errorMessage,
        };

        if (typeof errorMessage === 'object' && errorMessage.length) {
            const error = errorMessage.map((message) => ({
                ...errorDefault,
                message,
            }));

            errorDefault = error;
        }

        // response.status(status).json(Serializer.serializeError(errorDefault));
        // disable json api for api v1
        if (isAcceptedApiV1) {
            const errorDefaultApiV1: any = {
                statusCode: status,
                result: false,
                method: method,
                path: url,
                message: errorMessage,
            };
            response.status(status).json(errorDefaultApiV1);
        } else {
            response
                .status(status)
                .json(Serializer.serializeError(errorDefault));
        }
    }
}
