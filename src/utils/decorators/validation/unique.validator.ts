import { ERRMSG_DUPLICATE_CODE, ERRMSG_DUPLICATE_EMAIL, ERRMSG_DUPLICATE_PHONE_NUMBER, ERR_MSG_VALUE } from "@utils/ErrorCode/error-message";
import { ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";

@ValidatorConstraint({ name: 'UniqueEmail', async: false })
export class UniqueEmail implements ValidatorConstraintInterface {
    async validate(email: string, ValidationArguments: ValidationArguments) {
        const count = await ValidationArguments.constraints[0].count({
          where: {
            email,
            isDeleted: false,
          },
        });
    
        return !((count > 0));
      }
    
      defaultMessage(args: ValidationArguments) {
        return ERR_MSG_VALUE[ERRMSG_DUPLICATE_EMAIL].id;
      }
}

@ValidatorConstraint({ name: 'UniqueCode', async: false })
export class UniqueCode implements ValidatorConstraintInterface {
    async validate(code: string, ValidationArguments: ValidationArguments) {
        const count = await ValidationArguments.constraints[0].count({
          where: {
            code,
            isDeleted: false,
          },
        });
    
        return !((count > 0));
      }
    
      defaultMessage(args: ValidationArguments) {
        return ERR_MSG_VALUE[ERRMSG_DUPLICATE_CODE].id;
      }
}

@ValidatorConstraint({ name: 'UniquePhoneNumber', async: false })
export class UniquePhoneNumber implements ValidatorConstraintInterface {
    async validate(phoneNumber: string, ValidationArguments: ValidationArguments) {
        const count = await ValidationArguments.constraints[0].count({
          where: {
            phoneNumber,
            isDeleted: false,
          },
        });
    
        return !((count > 0));
      }
    
      defaultMessage(args: ValidationArguments) {
        return ERR_MSG_VALUE[ERRMSG_DUPLICATE_PHONE_NUMBER].id;
      }
}