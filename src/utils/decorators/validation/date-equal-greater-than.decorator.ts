import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';

export function IsEqualGreaterThan(
  property: string,
  validationOptions?: ValidationOptions,
) {
  return function (object: unknown, propertyName: string) {
    registerDecorator({
      name: 'IsEqualGreaterThan',
      target: object.constructor,
      propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          const [relatedPropertyName] = args.constraints;
          const relatedValue = args.object[relatedPropertyName];
          // console.log('>>> VALUE1: ' + value);
          // console.log('>>> VALUE2: ' + relatedValue);
          return value >= relatedValue;
        },
        defaultMessage({ property, constraints }) {
          return `${property} must be greater than ${constraints[0]}`;
        },
      },
    });
  };
}
