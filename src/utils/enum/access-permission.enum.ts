export enum ACCESS_PERMISSION {
    MANAGE = 'MANAGE',
    ACCESS = 'ACCESS',
    CREATE = 'CREATE',
    EDIT = 'EDIT',
    DELETE = 'DELETE',
  }
  