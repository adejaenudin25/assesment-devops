import { FileInterceptor } from '@nestjs/platform-express';
import { Injectable, mixin, NestInterceptor, Type } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MulterOptions } from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import { diskStorage } from 'multer';
import { extname, join } from 'path';
import * as moment from 'moment';
import MulterGoogleCloudStorage from 'multer-cloud-storage';

interface IEditFileNameExtras {
  fileNameExtras?(req: any, file: any, callback: any);
}
interface IUploadFileInterceptorOptions {
  fieldName: string;
  path?: string;
  fileFilter?: MulterOptions['fileFilter'];
  limits?: MulterOptions['limits'];
  fileNameExtras?: IEditFileNameExtras['fileNameExtras'];
}

export const editFileName = (req, file, callback) => {
  const name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const randomName = Array(16)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${name}-${randomName}-${Date.now()}${fileExtName}`);
};

function UploadFileInterceptor(
  options: IUploadFileInterceptorOptions,
): Type<NestInterceptor> {
  @Injectable()
  class Interceptor implements NestInterceptor {
    fileInterceptor: NestInterceptor;
    constructor(
      configService: ConfigService,
    ) {
      const filesDestination = configService.get<string>(
        'app.uploadedFileDestination',
      );

      const destination = join(filesDestination, options.path);

      const localStorage = diskStorage({
        destination,
        filename: options.fileNameExtras
          ? options.fileNameExtras
          : editFileName,
      });

      const multerOptions: MulterOptions = {
        storage: localStorage,
        fileFilter: options.fileFilter,
        limits: options.limits,
      };

      this.fileInterceptor = new (FileInterceptor(
        options.fieldName,
        multerOptions,
      ))();
    }

    intercept(...args: Parameters<NestInterceptor['intercept']>) {
      return this.fileInterceptor.intercept(...args);
    }
  }
  return mixin(Interceptor);
}

export default UploadFileInterceptor;
