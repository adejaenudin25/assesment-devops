import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
  HttpStatus,
  BadGatewayException,
  HttpException,
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

export interface Response<T> {
  statusCode: HttpStatus;
  result: boolean;
  message: string;
  method: string;
  path: string;
  payload: T;
}

@Injectable()
export class TransformInterceptor<T>
  implements NestInterceptor<T, Response<T>>
{
  intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Observable<Response<T>> {
    let contextHttp = context.switchToHttp().getResponse();
    let requestContext = context.switchToHttp().getRequest();
    return next.handle().pipe(
      map((data) => ({
        statusCode: contextHttp.statusCode,
        result: true,
        message: requestContext.statusMessage,
        method: requestContext.method,
        path: requestContext.url,
        payload: data,
      })),

      // catchError((err) => {
      //     console.log('error response: ', err.response);

      //     // throw new HttpException({
      //     //     status: HttpStatus.FORBIDDEN,
      //     //     error: 'This is a custom exception message',
      //     //   }, HttpStatus.FORBIDDEN);

      //     throw err.response; // throwing it for client
      // }),
    );
  }
}
