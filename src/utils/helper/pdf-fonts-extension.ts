import { join } from "path";

export const customFonts = {
  Nunito: {
    normal: join(process.cwd(), 'libs', 'fonts', 'Nunito', 'Nunito-Regular.ttf'),
    bold: join(process.cwd(), 'libs', 'fonts', 'Nunito', 'Nunito-Bold.ttf'),
    italics: join(process.cwd(), 'libs', 'fonts', 'Nunito', 'Nunito-Italic.ttf'),
    bolditalics: join(process.cwd(), 'libs', 'fonts', 'Nunito', 'Nunito-BoldItalic.ttf'),
  },
};
