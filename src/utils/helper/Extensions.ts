import * as moment from "moment";
import { Express } from 'express';

export default class Extensions {
    static utcNowRequest() { 
        const utcNow = new Date().toISOString();
        const utcNowReplaced = utcNow.replace('T', ' ').replace('Z', '');
        return new Date(utcNowReplaced);
    }
    
    static numberWithCommas(x: number) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    static diffYearMonthDate(firstDate: Date, secondDate: Date) {
        var a = moment(firstDate);
        var b = moment(secondDate);

        var years = a.diff(b, 'year');
        b.add(years, 'years');

        var months = a.diff(b, 'months');
        b.add(months, 'months');

        var days = a.diff(b, 'days');

        return {
            years: years,
            months: months,
            days: days
        };
    }

    static chunks<T>(data: T[], perChunk: number): T[][] {
        const result = data.reduce((resultArray, item, index) => { 
        const chunkIndex = Math.floor(index/perChunk);

        if(!resultArray[chunkIndex]) {
            resultArray[chunkIndex] = []; // start a new chunk
        }

        resultArray[chunkIndex].push(item);

        return resultArray;
        }, [])

        return result;
    }

    static toTitleCase = (phrase) => {
        return phrase
          .toLowerCase()
          .split(' ')
          .map(word => word.charAt(0).toUpperCase() + word.slice(1))
          .join(' ');
      };

    static getStringPathImage(file: Express.Multer.File): string {
        let pathFile = "";
            if (file.destination) {
                pathFile = `${file.destination}${file.filename}`;
            } else {
                pathFile = file.path;
            }
            
        return pathFile;
    }
}