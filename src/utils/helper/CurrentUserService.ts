export default class CurrentUserService {
    private  static _userLoginId: string;
    private  static _username: string;
 
    public  static get userLoginId() {
        return this._userLoginId;
    }

    public static set userLoginId(theUserLoginId: string) {
        this._userLoginId = theUserLoginId;
    }

    public static get username() {
        return this._username;
    }

    public  static set username(theUsername: string) {
        this._username = theUsername;
    }
}