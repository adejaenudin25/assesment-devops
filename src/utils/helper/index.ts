import {
  ClassTransformOptions,
  plainToClass,
  plainToInstance,
  Transform,
} from 'class-transformer';
import * as moment from 'moment';
import { Express } from 'express';

export const circularToJSON = (circular: unknown) =>
  JSON.parse(JSON.stringify(circular));

export function generateViewModel<T, V>(
  cls: { new (...args: any[]): T },
  obj: V[],
  options?: ClassTransformOptions,
): T[];
export function generateViewModel<T, V>(
  cls: { new (...args: any[]): T },
  obj: V,
  options?: ClassTransformOptions,
): T;
export function generateViewModel(...args: any[]) {
  const result = plainToInstance(args[0], circularToJSON(args[1]), {
    excludeExtraneousValues: true,
    exposeUnsetFields: false,
    enableImplicitConversion: true,
    ...args[2],
  });
  return result as unknown;
}

// export function generateViewModelListWithoutPagination

export function Default(defaultValue: unknown): PropertyDecorator {
  return Transform((obj: any) => obj?.value ?? defaultValue);
}

/**
 * @param date
 * @param format
 * @returns moment
 */
export const formatterDateNow = (date?: moment.Moment, format?: string) =>
  date ? moment(date).locale('id').format(format) : moment();

export const formatterDateTime = (date?: moment.Moment, format?: string) =>
  date ? moment(date).format(format) : moment();

export const capitalizeFirstLetter = (letter?: string) =>
  letter?.replace(/(^\w|\s\w)/g, (m) => m.toUpperCase());

export const getStringPathImage = (file: Express.Multer.File): string => {
  let pathFile = '';
  if (file.destination) {
    pathFile = `${file.destination}${file.filename}`;
  } else {
    pathFile = file.path;
  }

  return pathFile;
};

export const setTimeToLocalUtc = (date: string): string => {
  return moment(date, 'YYYY-MM-DD HH:mm:ss').utcOffset(7).format();
};

export const removeDotFromString = (text: string): string =>
  text.replace(/\./g, '');

export const stringifyError = (err, filter = null, space = "\t") : string => {
  var plainObject = {};
  Object.getOwnPropertyNames(err).forEach(function (key) {
    plainObject[key] = err[key];
  });
  return JSON.stringify(plainObject, filter, space);
};
  
export const removeZeroFromOrderNumber = (text: string): string => {
  const a = text.split('/');

  return `${Number(a[0])}${text.substring(a[0].length)}`;
}

export const formatNumber = (x: number): string => {
  return x.toLocaleString('id');
}

export const diffYearMonthDate = (firstDate: Date, secondDate: Date) => {
  var a = moment(firstDate);
  var b = moment(secondDate);

  var years = a.diff(b, 'year');
  b.add(years, 'years');

  var months = a.diff(b, 'months');
  b.add(months, 'months');

  var days = a.diff(b, 'days');

  return {
      years: years,
      months: months,
      days: days
  };
}

export const getAgeInMonth = (date: Date, EndDate: Date = moment().toDate()) => {
  // i day is 0.032855 month
  return moment(EndDate, 'YYYY-MM-DD').diff(moment(date, 'YYYY-MM-DD'), 'day') * 0.032855;
}

export const getAgeInMonthNumber = (date: Date, EndDate: Date = moment().toDate()) => {
  return moment(EndDate, 'YYYY-MM-DD').diff(moment(date, 'YYYY-MM-DD'), 'month');
}

export const getAgeInDay = (date: Date, EndDate: Date = moment().toDate()) => {
  return moment(EndDate, 'YYYY-MM-DD').diff(moment(date, 'YYYY-MM-DD'), 'day');
}

export const isAdult = (date: Date) => {
  return getAgeInMonth(date) >= (19*12);
}

export const nameof = (obj, expression) => {
  var res = {};
  Object.keys(obj).map(k => { res[k] = () => k; });
  return expression(res)();
}

export const jsonToUnpivot = (obj) => {
  return obj.reduce(reducer, []);
}

export const reducer = (acc, x) => {
  const findOrAdd = key => {
    let item = acc.find(v => v.cluster === key);
    if (item) return item;
    item = {
      cluster: key
    };
    acc.push(item);
    return item;
  }

  Object.keys(x).filter(key => key !== 'split')
    .forEach(key => {
      const item = findOrAdd(key);
      item['value'] = x[key];
    });
  return acc;
}

/* Given a start date, end date and day name, return
** an array of dates between the two dates for the
** given day inclusive
** @param {Date} start - date to start from
** @param {Date} end - date to end on
** @param {string} dayName - name of day
** @returns {Array} array of Dates
*/
export const getDaysBetweenDates = (start: Date, end: Date, dayName: "sun" | "mon" | "tue" | "wed" | "thu" | "fri" | "sat") => {
  var result: Date[] = [];
  var days = {sun:0,mon:1,tue:2,wed:3,thu:4,fri:5,sat:6};
  var day = days[dayName];
  // Copy start date
  var current = new Date(start);
  // Shift to next of required days
  current.setDate(current.getDate() + (day - current.getDay() + 7) % 7);
  // While less than end date, add dates to result array
  while (current < end) {
    result.push(new Date(+current));
    current.setDate(current.getDate() + 7);
  }
  return result;  
}

export const romanize = (num: number) => {
  if (isNaN(num))
      return NaN;
  var digits = String(+num).split(""),
      key = ["","C","CC","CCC","CD","D","DC","DCC","DCCC","CM",
             "","X","XX","XXX","XL","L","LX","LXX","LXXX","XC",
             "","I","II","III","IV","V","VI","VII","VIII","IX"],
      roman = "",
      i = 3;
  while (i--)
      roman = (key[+digits.pop() + (i * 10)] || "") + roman;
  return Array(+digits.join("") + 1).join("M") + roman;
}

export const toFixedIfNecessary = (value: any, decimalPlace: number) => {
  return +parseFloat(value).toFixed( decimalPlace );
}

export const monthInDays = (value: number) => {
  // 1 month is 30.436875 day
  return value * 30.436875;
} 
