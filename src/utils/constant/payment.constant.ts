export const PAYMENT_BANK_TRANSFER_AVAILABLE = [
  'BCA',
  'BNI',
  'BSI',
  'BRI',
  'MANDIRI',
];

export const PAYMENT_MANUAL_TRANSFER_AVAILABLE = ['BCA', 'MANDIRI'];

export const PAYMENT_CREDIT_CARD_AVAILABLE = ['CREDIT_CARD'];
export const PAYMENT_PAYLATER_AVAILABLE = ['KREDIVO'];
export const PAYMENT_LANGUANGE_DEFAULT = 'id';
export const PAYMENT_DESCRIPTION_DEFAULT = 'Pembayaran Invoice Transaksi';
export const PAYMENT_SUCCESS_REDIRECT_URL = 'https://imuni.id/paymentsucess/';
export const PAYMENT_FAILURE_REDIRECT_URL = 'https://imuni.id/paymentfailed/';
