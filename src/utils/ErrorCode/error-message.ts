/**
 * General CRUD
 */
export const ERRMSG_DATA_NOT_EXIST = (entity) => `DATA ${entity} NOT EXIST`;

/**
 * Duplication section
 */
export const ERRMSG_DUPLICATE_NAME = 'ERRMSG_DUPLICATE_NAME';
export const ERRMSG_DUPLICATE_DISTRICT_NAME = 'ERRMSG_DUPLICATE_DISTRICT_NAME';
export const ERRMSG_DUPLICATE_EMAIL = 'ERRMSG_DUPLICATE_EMAIL';
export const ERRMSG_DOCTOR_DUPLICATE_EMAIL = 'ERRMSG_DOCTOR_DUPLICATE_EMAIL';
export const ERRMSG_DUPLICATE_CODE = 'ERRMSG_DUPLICATE_CODE';
export const ERRMSG_DUPLICATE_PHONE_NUMBER = 'ERRMSG_DUPLICATE_PHONE_NUMBER';
export const ERRMSG_DUPLICATE_USERNAME = 'ERRMSG_DUPLICATE_USERNAME';
export const ERRMSG_DUPLICATE_ORDER_NUMBER = 'ERRMSG_DUPLICATE_ORDER_NUMBER';
export const ERRMSG_DUPLICATE_DAYOPS_DATE = 'ERRMSG_DUPLICATE_DAYOPS_DATE';

/**
 * Upload file section
 */
export const ERRMSG_WRONG_FORMAT_IMAGE = 'ERRMSG_WRONG_FORMAT_IMAGE';

export const ERR_MSG_VALUE = {
  ERRMSG_DUPLICATE_EMAIL: {
    en: 'Email is already taken, try another',
    id: 'Email Dokter sudah tersedia',
  },
  ERRMSG_DOCTOR_DUPLICATE_EMAIL: {
    en: 'Email already registered as a doctorr',
    id: 'Email sudah terdaftar sebagai dokter',
  },
  ERRMSG_DUPLICATE_PHONE_NUMBER: {
    en: 'Phone is already taken, try another',
    id: 'Nomor Telpon sudah tersedia',
  },
  ERRMSG_DUPLICATE_CODE: {
    en: 'Code is already taken, try another',
    id: 'Kode dokter sudah tersedia',
  },
  ERRMSG_PRODUCT_SKU_NOT_FOUND: {
    en: 'Product not Found',
    id: 'Product tidak di temukan',
  },
  ERRMSG_NO_TRANS_MATCH_PURCHASE_TOKEN: {
    en: 'No transaction match purchase token',
    id: 'tidak ada transaksi yang cocok dengan purchase token',
  },
  ERRMSG_DUPLICATE_PURCHASE: {
    en: 'You has duplicate purchase',
    id: 'anda memiliki pembelian ganda',
  },
  ERRMSG_ALREADY_OWNED_PRODUCT: {
    en: 'You Already owned this product',
    id: 'kamu sudah memiliki product ini',
  },
  ERRMSG_ALREADY_SUBSCRIBE_PRODUCT: {
    en: 'Your subscription for this product still active',
    id: 'Langganann anda untuk produk ini masih aktif',
  },
  ERRMSG_EXPIRED_TOKEN: {
    en: 'Token expired',
    id: 'Token sudah kadaluarsa',
  },
  ERRMSG_INVALID_TOKEN: {
    en: 'Invalid token',
    id: 'Token tidak valid',
  },
  ERRMSG_INVALID_ADMIN_LOGIN_CRED: {
    en: 'Invalid email or password',
    id: 'Email atau kata sandi salah',
  },
  ERRMSG_INVALID_OLD_PASSWORD: {
    en: 'Invalid Old Password',
    id: 'Kata sandi lama tidak valid',
  },
  ERRMSG_USER_NOT_EXIST: {
    en: 'User does not exist',
    id: 'Pengguna tidak ada',
  },
  ERRMSG_SCHEDULED_CONTENT_NOT_FOUND: {
    en: 'Content does not exist',
    id: 'Konten tidak ada',
  },
  ERRMSG_BOOK_CHALLENGE_NOT_FOUND: {
    en: 'Challenge does not exists or has been canceled',
    id: 'Tantangan tidak ada atau telah dibatalkan',
  },
  ERRMSG_BOOK_CLASS_NOT_FOUND: {
    en: 'Class does not exists or has been canceled',
    id: 'Kelas tidak ada atau telah dibatalkan',
  },
  ERRMSG_BOOK_PRODUCT_TYPE_NOT_FOUND: {
    en: "Can't process booking for this product type",
    id: 'Tidak dapat memproses pemesanan untuk jenis produk ini',
  },
  ERRMSG_ACTIVITY_NOT_FOUND: {
    en: 'Activity does not exists or has been deleted',
    id: 'Aktivitas tidak ada atau telah dihapus',
  },
  ERRMSG_DUPLICATE_NAME: {
    en: 'Name is taken, try another',
    id: 'Nama sudah digunakan, silakan gunakan yang lain',
  },
  ERRMSG_DUPLICATE_USERNAME: {
    en: 'That username is already taken, try another',
    id: 'Nama pengguna sudah digunakan, silakan gunakan yang lain',
  },
  ERRMSG_INVALID_DATE_FORMAT: {
    en: 'Date is not accepted',
    id: 'Tanggal tidak diterima',
  },
  ERRMSG_INVALID_NAME_FORMAT: {
    en: 'Name must be alphabets, more than two characters and only one word',
    id: 'Nama harus alfabet, lebih dari dua karakter atau hanya satu kata',
  },
  ERRMSG_INVALID_SIMPLE_PASSWORD_FORMAT: {
    en: 'Password must be at least 8 characters, including alphabets and numbers',
    id: 'Kata sandi minimal 8 karakter, termasuk alfabet dan angka',
  },
  ERRMSG_INVALID_PHONE_FORMAT: {
    en: 'Phone number format is not valid',
    id: 'Format nomor telepon tidak valid',
  },
  ERRMSG_BOOK_LIVE_CLASS_WITHOUT_SUBS: {
    en: 'This is a premium content',
    id: 'Ini adalah konten premium',
  },
  ERR_MSG_NO_BOOK_DATA: {
    en: 'No book data for this content',
    id: 'Tidak ada data pemesanan untuk konten ini',
  },
  ERRMSG_CHALLENGE_FINISHED_CANT_BOOK: {
    en: 'This challenge has been finished',
    id: 'Tantangan ini telah selesai',
  },
  ERRMSG_CHALLENGE_NO_CLASS_CANT_BOOK: {
    en: 'This challenge doesnt have class yet',
    id: 'Tantangan ini belum memiliki kelas',
  },
  ERRMSG_DUPLICATE_ORDER_NUMBER: {
    en: 'Order number is already taken, try another',
    id: 'Nomor order sudah digunakan, silakan gunakan yang lain',
  },
  ERRMSG_DUPLICATE_DISTRICT_NAME: {
    en: 'Data District Already Exist',
    id: 'Kecamatan sudah digunakan',
  },
  ERRMSG_DUPLICATE_DAYOPS_DATE: {
    en: 'Data Dayops Already Exist',
    id: 'Data Dayops sudah tersedia',
  },
};
